import 'package:firebase_photo_uploader/pages/upload_image_for_record.dart';
import 'package:firebase_photo_uploader/pages/view_images_for_record.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:firebase_photo_uploader/my_http.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart';
import 'dart:convert' as convert;

class AllRecords extends StatefulWidget {

  Widget internetData;

  @override
  _AllRecordsState createState() => _AllRecordsState();
}

class _AllRecordsState extends State<AllRecords> {
  final spinkit = SpinKitRotatingCircle(
    color: Colors.blue,
    size: 50.0,
  );

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    this.reloadData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("hello"),
      ),
      body: Builder(builder: (context) {
        return Column(
          children: <Widget>[
            Card(
              child: Padding(
                padding: const EdgeInsets.only(
                  left: 10,
                  right: 0,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Expanded(
                        child: Text("All Records"),
                    ),
                    Expanded(
                      child: IconButton(
                        icon: Icon(Icons.person_add),
                        onPressed: () {

                        },
                        color: Colors.blue,
                        tooltip: "Add new",
                      ),
                    )
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                height: MediaQuery.of(context).size.height - 170,
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      color: Colors.blueAccent,
                      width: 2,
                    ),
                    top: BorderSide(color: Colors.blueAccent),
                  ),
                ),
                child: widget.internetData,
              ),
            )
          ],
        );
      }),
    );
  }

  void reloadData() {
    setState(() {
      widget.internetData = this.getData();
    });
  }

  Widget getData() {
    return StreamBuilder(
      stream:
      myGet("user_datas").asStream(),
      builder: (context, snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
            return Text('Loading paused');
          case ConnectionState.active:
          case ConnectionState.waiting:
            return Center(
              child: spinkit,
            );
          case ConnectionState.done:
            if (snapshot.hasError) {
              return Text('Error: ${snapshot.error}');
            }
             print(snapshot.data.body);

            var jsonResponse = convert.jsonDecode(snapshot.data.body);
            dynamic data = jsonResponse['data'];
            if (data.length == 0) {
              return Card(
                child: Center(
                  child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            "No records found",
                            style: TextStyle(
                              fontWeight: FontWeight.w700,
                              fontSize: 17,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              );
            }
            return ListView.builder(
              itemCount: data.length,
              itemBuilder: (context, index) {
//                print(data[index]);
//                return Text("hey");
                return UserListItem(
                  data: data[index],
                  reloadData: this.reloadData,
                );
              },
            );
        }
        return Text("Something went wrong");
      },
    );
  }
}











class UserListItem extends StatelessWidget {
  final dynamic data;
  final Function changeIndex;
  final Function reloadData;
  UserListItem({this.data, this.changeIndex, this.reloadData});

  @override
  Widget build(BuildContext context) {
    if (data == null) {
      return Text("No data");
    }
    return Card(
      child: Padding(
        padding: const EdgeInsets.only(
          top: 10,
          bottom: 5,
          left: 20,
          right: 20,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "${data['name']} ${data['lastname']}",
                  style: TextStyle(
                    fontWeight: FontWeight.w700,
                    fontSize: 17,
                  ),
                ),
                Text(
                  "${data['email']}",
                  style: TextStyle(fontWeight: FontWeight.w300,),
                ),
                Text(
                  "Joined : ${data['created_at']}",
                  style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.w600,
                    fontSize: 12,),
                ),
              ],
            ),
            PopupMenuButton(
              onSelected: (value) async {
                String option = value.toString().split(":")[0];
                int id = int.parse(value.toString().split(":")[1].split(',')[0]);
                String username = value.toString().split(":")[1].split(',')[1];
                print(value);
                if (option == "images") {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => ImagesForRecord(id,username),
                    ),
                  );
                } else if (option == "delete") {
                  Response response = await myDelete("companyStaff/$id/remove");
                  print(response.statusCode);
                  print(response.body);
                  this.reloadData();
                } else if (option == "addImage") {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => ImageCapture(id,username),
                    ),
                  );
                }

                // print(value.toString().split(":"));
              },
              icon: Icon(FontAwesomeIcons.ellipsisV),
              itemBuilder: (context) {
                List<PopupMenuItem> pops = new List();
                pops.add(PopupMenuItem(
                  child: Text("View Images"),
                  value: "images:${data['id']},${data['username']}",
                ));

                 pops.add(PopupMenuItem(
                   child: Text("Add Image"),
                   value: "addImage:${data['id']},${data['username']}",
                 ));

                pops.add(
                  PopupMenuItem(
                    child: Text("Delete"),
                    value: "delete:${data['id']},${data['username']}",
                  ),
                );

                return pops;
              },
            )
          ],
        ),
      ),
    );
  }
}

